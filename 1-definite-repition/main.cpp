#include <iostream>
using namespace std;

int main() {

    // 10, 9, 8, 7, 6, 5

    int x;
    cin >> x;

    for (int i = x; i >= 5; i--) {
        cout << i << endl;
    }

	return 0;
}