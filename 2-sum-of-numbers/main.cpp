#include <iostream>
using namespace std;

int main() {

    // 1 2 3 4 5
    // 1 2 3

    /*
        Create a program that computes the sum of all inputted numbers.
    */

    int x;
    int total = 0;

    while (cin >> x) {
        total += x;
    }

    cout << "Total: " << total << endl;

	return 0;
}