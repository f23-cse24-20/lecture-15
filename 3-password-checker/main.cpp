#include <iostream>
using namespace std;

int main() {

    /*
        Create a program that checks if the user inputted the correct password.
        The user should get an unlimited number of attempts.
    */

    string password = "abc123";
    string input;

    while (cin >> input) {
        if (input == password) {
            cout << "Access granted" << endl;
            break;
        } else {
            cout << "Access denied" << endl;
        }
    }

	return 0;
}