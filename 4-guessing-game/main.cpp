#include <iostream>
#include <ucm_random>
using namespace std;

int main() {

    /*
        Create a game that generates a random number and lets the user guess it.
        The user should get an unlimited number of attempts.
    */

    RNG generator;
    int target = generator.get(1, 10);

    int guess;
    while (cin >> guess) {
        if (guess == target) {
            cout << "You guessed it!" << endl;
            break;
        } else {
            cout << "That is incorrect." << endl;
        }
    }

    cout << "Thank you for playing" << endl;


	return 0;
}